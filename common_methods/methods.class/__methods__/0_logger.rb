#
# Description: <Method description here>
#

class String
  def default;        "\e[39m#{self}\e[0m" end
  def black;          "\e[30m#{self}\e[0m" end
  def red;            "\e[31m#{self}\e[0m" end
  def green;          "\e[32m#{self}\e[0m" end
  def brown;          "\e[33m#{self}\e[0m" end
  def orange;         "\e[33m#{self}\e[0m" end
  def yellow;         "\e[33m#{self}\e[0m" end
  def blue;           "\e[34m#{self}\e[0m" end
  def magenta;        "\e[35m#{self}\e[0m" end
  def cyan;           "\e[36m#{self}\e[0m" end
  def gray;           "\e[37m#{self}\e[0m" end

  def bg_default;     "\e[49m#{self}\e[0m" end
  def bg_black;       "\e[40m#{self}\e[0m" end
  def bg_red;         "\e[41m#{self}\e[0m" end
  def bg_green;       "\e[42m#{self}\e[0m" end
  def bg_brown;       "\e[43m#{self}\e[0m" end
  def bg_orange;      "\e[43m#{self}\e[0m" end
  def bg_yellow;      "\e[43m#{self}\e[0m" end
  def bg_blue;        "\e[44m#{self}\e[0m" end
  def bg_magenta;     "\e[45m#{self}\e[0m" end
  def bg_cyan;        "\e[46m#{self}\e[0m" end
  def bg_gray;        "\e[47m#{self}\e[0m" end

  def bold;           "\e[1m#{self}\e[22m" end
  def italic;         "\e[3m#{self}\e[23m" end
  def underline;      "\e[4m#{self}\e[24m" end
  def blink;          "\e[5m#{self}\e[25m" end
  def reverse_color;  "\e[7m#{self}\e[27m" end
end

@debug_methods=nil
def _e0_initialize()
  instance=$evm.instantiate("/common_methods/methods/0_logger")
  @debug_methods=instance.attributes["states_in_debug_mode"] || []
  
  @object_type=$evm.root["vmdb_object_type"] rescue ""
  @vmdb_object=$evm.root[@object_type] rescue ""
  @object_id=$evm.root["#{@object_type}_id"] rescue ""
  @object_name=$evm.root["object_name"] rescue ""
  
  @provision_type=$evm.root["provision_type"] rescue ""
  
  @current_step=$evm.root["ae_state_step"] rescue ""
  @current_state=$evm.root["ae_state"] rescue ""
  
  @debug_methods
end

def e0_log_attributes()
  @debug_methods = @debug_methods || _e0_initialize()
    $evm.log(:info, " CC- methods=#{@debug_methods}")
  if @debug_methods.include?@current_state
    $evm.root.attributes.sort.each { |k,v| $evm.log(:info, "DEBUG: ROOT[#{k}]=#{v}") } rescue nil
    $evm.object.attributes.sort.each { |k,v| $evm.log(:info, "DEBUG: OBJECT[#{k}]=#{v}") } rescue nil
    $evm.root["ae_vmdb_type"].options.sort.each { |k,v| $evm.log(:info, "DEBUG: OPTIONS[#{k}]=#{v}") } rescue nil
  end
end



def e0_log(fonction="main", level="info", message="",color="default")
  @debug_methods = @debug_methods || _e0_initialize()
  case level.upcase
    when "INFO"
      lvl="INFO ".green
      msg=message
    when "ERROR"
      lvl="ERROR".red
    msg=message.bold
    when "WARN"
      lvl="WARN ".brown
      msg=message
    when "DEBUG"
      lvl="DEBUG".blue
      msg=message.italic
    else
    lvl=level.upcase
    msg=message.default
  end
  
  
  
  log="#{lvl.bold}: [#{@object_type}_#{@object_id}] [#{@current_state}/#{@current_step}] [#{@object_name}:#{fonction}] : #{msg}"
        
  $evm.log(:info, log) unless (level.downcase == "debug" && (!@debug_methods.include?(@current_state) && !@debug_methods.include?(@object_name)))
end


