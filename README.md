This datastore provide an embeded method that offers logging functions:

e0_log(<function>,<level>,<message>)
  function: function name that help to identify part of the code
  level: can be "info","debug" or any else. "debug" is a special level that is explained in next chapter
  message: text message that will be logged

e0_log_attributes
  log root attributes, object attributes and prov.options when in debug mode

the 2 functions prints logs for any level except "debug".
the "debug" level logs are prined only if the name of the step or method is listed in the attribute "states_in_debug_mode" of the instance

-----------------
# Log_enhancer # 

Access: newby
Fomat: embedded method

## Problematics: ##
When coding automation methods, it is usefull to log some informations, but some informations are for informations and some are for debugging when the method goes wrong, and some warning or error information. 

With default the logging mechanism it is very hard to follow logs for a specific provisioning, some informations like provision ID, method or the function the log come from.

An other problem is, when you want to debug a method, you have to switch the log level to debug for all appliances and all methods, as well the automation methods as the core methods.

## Solution: ##
The solution is to use an embedded method to wrap the logging mechanism:
> * It formats the log line:
> "#{lvl.bold}: [#{@object_type}_#{@object_id}] [#{@current_state}/#{@current_step}] [#{@object_name}:#{fonction}] : #{msg}"

> * It colorize the log to make more easy to read it
> > - lvl="INFO ".green
> > - lvl="ERROR".red ; msg=message.bold
> > - lvl="WARN ".brown
> > - lvl="DEBUG".blue ; msg=message.italic

> * It enable the debugging for specifics methods and steps
The list of the steps and methods can be set in the states_in_debug_mode attribute of the 0_logger instance
> * It provides public colorize class to enhance log lines
The string class is surcharged so every strings can be colorized with foreground color, background color or style:

## How to use : ##
2 functions are availlable:
> * e0_log_attributes:
> Prints, in automation.log when in debug mode a formated version of :
> > * $evm.root
> > * $evm.object
> > * $evm.root["ae_vmdb_type"].options
> * e0_log(fonction="main", level="info", message="",color="default")
Print in automation.log the formated log.
